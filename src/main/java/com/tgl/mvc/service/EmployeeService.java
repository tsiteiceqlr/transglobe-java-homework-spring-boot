package com.tgl.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.mvc.dao.EmployeeDao;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	public Integer insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.insert(employee);
	}

	public boolean delete(int employeeId) {
		return employeeDao.delete(employeeId);
	}
	
	public boolean update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = employeeDao.findById(employeeId);
		if (result == null) {
			return null;
		}
//		String chinessName = result.getChinessName();
		String maskedName = DataUtil.maskChinessName(result.getChinessName());
		result.setChinessName(maskedName);
		return result;
	}

}

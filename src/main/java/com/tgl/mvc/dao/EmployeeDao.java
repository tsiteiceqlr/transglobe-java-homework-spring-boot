package com.tgl.mvc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tgl.mvc.mapper.EmployeeRowMapper;
import com.tgl.mvc.model.Employee;

@Repository
public class EmployeeDao {

	private static final String INSERT = "INSERT INTO employee (HEIGHT, WEIGHT, ENGLISH_NAME, CHINESS_NAME, PHONE, EMAIL, BMI) VALUES (:height, :weight, :englishName, :chinessName, :phone, :email, :bmi)";

	private static final String DELETE = "DELETE FROM employee WHERE ID=:id";

	private static final String UPDATE = "UPDATE employee SET HEIGHT=:height, WEIGHT=:weight, ENGLISH_NAME=:englishName, CHINESS_NAME=:chinessName, PHONE=:phone, EMAIL=:email, BMI=:bmi WHERE ID=:id";

	private static final String SELECT = "SELECT ID, HEIGHT, WEIGHT, ENGLISH_NAME, CHINESS_NAME, PHONE, EMAIL, BMI FROM employee";

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Integer insert(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee), keyHolder);
		return keyHolder.getKey().intValue();
	}

	@CacheEvict(value = "empFindById", key = "#employeeId")
	public boolean delete(int employeeId) {
		return namedParameterJdbcTemplate.update(DELETE, new MapSqlParameterSource("id", employeeId)) > 0;
	}
	
	@CachePut(value = "empFindById", key = "#employee.id")
	public boolean update(Employee employee) {
		return namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(employee)) > 0;
	}

	@Cacheable(value = "empFindById", key = "#employeeId")
	public Employee findById(int employeeId) {
		try {
			return namedParameterJdbcTemplate.queryForObject(SELECT + " WHERE ID=:id",
					new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			// LOG.error("Metric getMetricByID failed, id: {}, Exception: {}", employeeId,
			// e);
		}
		return null;
	}

}

package com.tgl.mvc.util;

public class DataUtil {

	private DataUtil() {}

	public static String maskChinessName(String chinessName) {
		if (chinessName == null || chinessName.length() == 0) {
			return "";
		}

		StringBuilder mask = new StringBuilder();
		mask.append(chinessName.substring(0, 1));
		mask.append("*");
		if (chinessName.length() == 3) {
			mask.append(chinessName.substring(2, 3));
		}
		if (chinessName.length() == 4) {
			mask.append("*");
			mask.append(chinessName.substring(3, 4));
		}
		return mask.toString();
	}

	public static float bmi(int height, int weight) {
		return ((float) weight / (float) Math.pow(((float) height / 100), 2));
	}

}

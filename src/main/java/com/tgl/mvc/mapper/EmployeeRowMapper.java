package com.tgl.mvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.tgl.mvc.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {
	
	private static final String ID = "ID";
	private static final String HEIGHT = "HEIGHT";
	private static final String WEIGHT = "WEIGHT";
	private static final String ENGLISH_NAME = "ENGLISH_NAME";
	private static final String CHINESS_NAME = "CHINESS_NAME";
	private static final String PHONE = "PHONE";
	private static final String EMAIL = "EMAIL";
	private static final String BMI = "BMI";

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee employee = new Employee();
		employee.setId(rs.getInt(ID));
		employee.setHeight(rs.getInt(HEIGHT));
		employee.setWeight(rs.getInt(WEIGHT));
		employee.setEnglishName(rs.getString(ENGLISH_NAME));
		employee.setChinessName(rs.getString(CHINESS_NAME));
		employee.setPhone(rs.getString(PHONE));
		employee.setEmail(rs.getString(EMAIL));
		employee.setBmi(rs.getFloat(BMI));
		return employee;
	}

}
